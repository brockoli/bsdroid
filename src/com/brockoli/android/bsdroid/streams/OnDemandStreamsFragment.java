/*
   Copyright [2012] [Rob Woods]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.brockoli.android.bsdroid.streams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.brockoli.android.bsdroid.HSDroidSettings;
import com.brockoli.android.bsdroid.R;
import com.brockoli.android.bsdroid.datamodel.BaseStream;
import com.brockoli.android.bsdroid.datamodel.LiveStream;
import com.brockoli.android.bsdroid.service.RESTService;
import com.brockoli.android.bsdroid.streams.StreamsFragment.OnStreamButtonClickedListener;
import com.brockoli.android.bsdroid.utils.GeneralUtils;

public class OnDemandStreamsFragment extends StreamsFragment implements DatePicker.OnDateChangedListener, 
OnStreamButtonClickedListener, 
ExpandableListView.OnGroupExpandListener,
ActionBar.OnNavigationListener {
	private static final String TEAMS_LIST = "TEAMS_LIST";
	private static final String LAST_SELECTED_TEAM = "LAST_SELECTED_TEAM";
	private static final String LAST_VIEW_PICKED = "LAST_VIEW_PICKED";
	private static final String LAST_CALENDAR_SHOWING = "LAST_CALENDAR_SHOWING";
	private static final String LAST_PICKED_DAY = "LAST_PICKED_DAY";
    private static final String LAST_PICKED_MONTH = "LAST_PICKED_MONTH";
    private static final String LAST_PICKED_YEAR = "LAST_PICKED_YEAR";
	private static final int FADE_OUT_DURATION = 300;
	private static final int FADE_IN_DURATION = 300;
	private int mLastExpandedGroupId = -1;

	private DatePicker mDp;
	private TeamsAdapter mTeamsAdapter;
	
	// Used to track if the last view was set by team or by date
	private Boolean byTeam = true;
	private Boolean defaultNav = false;
	private Boolean calShowing = false;
	
	// Last picked date
	private int mLastPickedDay = -1;
	private int mLastPickedMonth = -1;
	private int mLastPickedYear = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			ArrayList<String> teamsList = savedInstanceState.getStringArrayList(TEAMS_LIST);
			if (teamsList != null) {
				mTeamsAdapter = new TeamsAdapter(teamsList);
				int position = savedInstanceState.getInt(LAST_SELECTED_TEAM, -1);
				setupActionBar(position);
			}			
		} else {
			// Get list of teams from hockeystreams.com
			new VODTeamsAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_LIST_TEAMS, null, RESTService.GET));						
		}

		showCalendar(calShowing);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View showsView = inflater.inflate(R.layout.vod_streams, null);

		setupBGTouch(showsView);

		mLv = (ExpandableListView) showsView.findViewById(R.id.stream_list);
		mLv.setOnGroupExpandListener(this);

		// Set the list adapter if coming from a savedInstance
		if (savedInstanceState != null) {
			mLastExpandedGroupId = savedInstanceState.getInt(LAST_EXPANDED_GROUP_ID);
			byTeam = savedInstanceState.getBoolean(LAST_VIEW_PICKED, true);
			calShowing = savedInstanceState.getBoolean(LAST_CALENDAR_SHOWING, false);
			mLastPickedDay = savedInstanceState.getInt(LAST_PICKED_DAY, -1);
			mLastPickedMonth = savedInstanceState.getInt(LAST_PICKED_MONTH, -1);
			mLastPickedYear = savedInstanceState.getInt(LAST_PICKED_YEAR, -1);
			List streams = savedInstanceState.getParcelableArrayList(PREVIOUS_LIST_DATA);
			if (streams != null) {
				ExpandableStreamAdapter adapter = new ExpandableStreamAdapter(getActivity(), streams, false);
				adapter.setOnStreamButtonClickedListener(this);
				mLv.setAdapter(adapter);				
			}
		}
		
		setupDatePicker(showsView);
		return showsView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(LAST_EXPANDED_GROUP_ID, mLastExpandedGroupId);
		outState.putBoolean(LAST_VIEW_PICKED, byTeam);
		outState.putBoolean(LAST_CALENDAR_SHOWING, calShowing);
		outState.putInt(LAST_PICKED_DAY, mLastPickedDay);
		outState.putInt(LAST_PICKED_MONTH, mLastPickedMonth);
		outState.putInt(LAST_PICKED_YEAR, mLastPickedYear);
		if (mTeamsAdapter != null && !mTeamsAdapter.isEmpty()) {
			outState.putStringArrayList(TEAMS_LIST, mTeamsAdapter.getList());
			ActionBar actionbar = getActivity().getActionBar();
			outState.putInt(LAST_SELECTED_TEAM, actionbar.getSelectedNavigationIndex());
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.add("").setIcon(R.drawable.collections_go_to_today).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			if (mDp.getVisibility() == View.VISIBLE) {
				showCalendar(false);
			} else {
				showCalendar(true);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private class VODStreamsAsyncTask extends StreamsAsyncTask {

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				List streams = new ArrayList<LiveStream>();
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						JSONArray jsonStreams = jsonResponse.getJSONArray("ondemand");
						for (int i=0; i < jsonStreams.length(); i++) {
							JSONObject jsonStream = jsonStreams.getJSONObject(i);
							String id = jsonStream.getString("id");
							String event = jsonStream.getString("event");
							String homeTeam = jsonStream.getString("homeTeam");
							String awayTeam = jsonStream.getString("awayTeam");
							int flash = jsonStream.getInt("isFlash");
							Boolean isFlash = (flash == 0) ? false : true;
							int wmv = jsonStream.getInt("isWMV");
							Boolean isWmv = (wmv == 0) ? false : true;
							int istream = jsonStream.getInt("isiStream");
							Boolean isIStream = (istream == 0) ? false : true;
							BaseStream baseStream = new BaseStream(id, event, homeTeam, awayTeam, isFlash, isWmv, isIStream, false);
							streams.add(baseStream);
						}
					} else {
						Toast.makeText(OnDemandStreamsFragment.this.getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				ExpandableStreamAdapter adapter = new ExpandableStreamAdapter(getActivity(), streams, false);
				adapter.setOnStreamButtonClickedListener(OnDemandStreamsFragment.this);
				mLv.setAdapter(adapter);
			}				
		}
	}

	private class VODDatesAsyncTask extends StreamsAsyncTask {

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						JSONArray dates = jsonResponse.getJSONArray("dates");
						if (dates != null) {
							Calendar date = GeneralUtils.getCalendar(dates.getString(0));
							Log.d("HSDROID", "year: " + date.get(Calendar.YEAR) + " month: " + date.get(Calendar.MONTH) + " day: " + date.get(Calendar.DAY_OF_MONTH));
							mDp.init(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH), OnDemandStreamsFragment.this);
							String maxDate = dates.getString(0);
							String minDate = dates.getString(dates.length()-1);
							mDp.setMinDate(GeneralUtils.parseDate(minDate));	
							mDp.setMaxDate(date.getTimeInMillis());
							mDp.updateDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}		
	}

	private class VODTeamsAsyncTask extends StreamsAsyncTask {

		@Override
		protected void onPostExecute(String result) {
			if (result != null) {
				ArrayList<String> teamList = new ArrayList<String>();
				try {
					JSONObject jsonResponse = (JSONObject) new JSONTokener(result).nextValue();
					if (jsonResponse.getString("status").equals("Success")) {
						JSONArray teams = jsonResponse.getJSONArray("teams");
						if (teams != null) {
							for (int i=0; i<teams.length(); i++) {
								JSONObject team = teams.getJSONObject(i);
								teamList.add(team.getString("name"));
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (teamList != null && !teamList.isEmpty()) {
					mTeamsAdapter = new TeamsAdapter(teamList);
					setupActionBar(0);
				}
			}
		}		
	}

	private void setupDatePicker(View view) {	
		mDp = (DatePicker) view.findViewById(R.id.datepicker);

		if (mLastPickedYear > 0) {
			mDp.init(mLastPickedYear, mLastPickedMonth, mLastPickedDay, this);
		} else {
			new VODDatesAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_GET_ON_DEMAND_DATES, null, RESTService.GET));			
		}
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		mLastPickedDay = dayOfMonth;
		mLastPickedMonth = monthOfYear;
		mLastPickedYear = year;
		byTeam = false;
		ContentValues cv = new ContentValues();
		cv.put(RESTService.REST_PARAM_DATE, GeneralUtils.formatDate(year, monthOfYear+1, dayOfMonth));
		new VODStreamsAsyncTask().execute(RESTService.instance()
				.createRestIntent(RESTService.REST_METHOD_GET_ON_DEMAND, cv, RESTService.GET));
		
	}

	private void setupActionBar(int position) {
		ActionBar actionbar = getActivity().getActionBar();
		// actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		defaultNav = true;
		actionbar.setListNavigationCallbacks(mTeamsAdapter, this);
		if (actionbar.getNavigationMode() == ActionBar.NAVIGATION_MODE_LIST && position > -1 && byTeam) {
			actionbar.setSelectedNavigationItem(position);			
		}
	}

	private void setupBGTouch(View v) {
		View bg = (View) v.findViewById(R.id.main_view);
		bg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showCalendar(false);
			}
		});
	}

	private void showCalendar(Boolean show) {
		float height = mDp.getHeight();
		if (show && mDp.getVisibility() != View.VISIBLE) {
			calShowing = true;
			ObjectAnimator animCal = ObjectAnimator.ofFloat(mDp, "translationY", -height, 0f);
			animCal.setDuration(FADE_IN_DURATION);
			animCal.setInterpolator(new DecelerateInterpolator());
					
			animCal.addListener(new AnimatorListener() {

				@Override
				public void onAnimationStart(Animator animation) {
					//mDp.setAlpha(0f);
					mDp.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAnimationRepeat(Animator animation) {}

				@Override
				public void onAnimationEnd(Animator animation) {}

				@Override
				public void onAnimationCancel(Animator animation) {}
			});
			
			animCal.start();			
		} else if (!show && mDp.getVisibility() == View.VISIBLE){
			calShowing = false;
			ObjectAnimator animCal = ObjectAnimator.ofFloat(mDp, "translationY", 0f, -height);
			animCal.setDuration(FADE_OUT_DURATION);
			animCal.setInterpolator(new AccelerateInterpolator());

			animCal.addListener(new AnimatorListener() {

				@Override
				public void onAnimationStart(Animator animation) {}

				@Override
				public void onAnimationRepeat(Animator animation) {}

				@Override
				public void onAnimationEnd(Animator animation) {
					mDp.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation) {}
			});
			
			animCal.start();
		}
	}

	private class TeamsAdapter implements SpinnerAdapter {

		ArrayList<String> data;

		public TeamsAdapter(ArrayList<String> teams) {
			this.data = teams;
		}

		@Override
		public int getCount() {
			return data.size();
		}

		@Override
		public Object getItem(int position) {
			return data.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public int getItemViewType(int position) {
			return android.R.layout.simple_spinner_dropdown_item;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView v = new TextView(getActivity());
			v.setTextColor(getResources().getColor(android.R.color.white));
			v.setTextSize(18.0f);
			v.setText(data.get(position).toUpperCase());
			return v;
		}

		@Override
		public int getViewTypeCount() {
			return 1;
		}

		@Override
		public boolean hasStableIds() {
			return false;
		}

		@Override
		public boolean isEmpty() {
			return data.isEmpty();
		}

		@Override
		public void registerDataSetObserver(DataSetObserver observer) {
			// TODO Auto-generated method stub

		}

		@Override
		public void unregisterDataSetObserver(DataSetObserver observer) {

		}

		@Override
		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			TextView v = new TextView(getActivity());
			v.setTextColor(getResources().getColor(android.R.color.black));
			v.setBackgroundColor(getResources().getColor(R.color.hs_purple));
			v.setText(data.get(position));
			return v;
		}

		public ArrayList<String> getList() {
			return data;
		}
	}

	@Override
	public void onStreamButtonClicked(BaseStream stream, String type, Boolean isHd) {
		// Toast.makeText(getActivity(), "type: " + type + " id: " + stream.getId(), Toast.LENGTH_SHORT).show();

		// Slight hack here to pass the selected stream format to our AsyncTask so we can filter on it
		// Create a second intent with the stream type as an extra and pass it to our AsyncTask along
		// with the intent created for the REST service

		Intent streamTypeIntent = new Intent();
		streamTypeIntent.putExtra(STREAM_TYPE, type);
		streamTypeIntent.putExtra(STREAM_QUALITY, isHd);

		ContentValues cv = new ContentValues();
		cv.put(RESTService.REST_PARAM_STREAM_ID, stream.getId());

		// Check settings to see if a preferred server location is set and use it
		boolean useLocation = HSDroidSettings.getInstance().locationFlag(getActivity());
		String location;
		if (useLocation) {
			location = HSDroidSettings.getInstance().location(getActivity());
			cv.put(RESTService.REST_PARAM_LOCATION, location);
		}

		new StreamAsyncTask().execute(RESTService.instance()
				.createRestIntent(RESTService.REST_METHOD_GET_ON_DEMAND_STREAM, cv, RESTService.GET), streamTypeIntent);

	}

	@Override
	public void onGroupExpand(int groupPosition) {
		if (mLastExpandedGroupId < mLv.getCount() && mLastExpandedGroupId != groupPosition) {
			mLv.collapseGroup(mLastExpandedGroupId);
			mLastExpandedGroupId = groupPosition;
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if (!defaultNav) {
			showCalendar(false);
			ContentValues cv = new ContentValues();
			cv.put(RESTService.REST_PARAM_TEAM, (String) mTeamsAdapter.getItem(itemPosition));
			new VODStreamsAsyncTask().execute(RESTService.instance()
					.createRestIntent(RESTService.REST_METHOD_GET_ON_DEMAND, cv, RESTService.GET));
			
		}
		defaultNav = false;
		return true;
	}
}
