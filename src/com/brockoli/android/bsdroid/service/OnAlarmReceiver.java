package com.brockoli.android.bsdroid.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnAlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		WakefulIntentService.acquireStaticLock(context);
		Intent i = new Intent(context, NotificationService.class);
		i.putExtra(NotificationService.SEND_NOTIFICATIONS, true);
		context.startService(i);
	}
}
