package com.brockoli.android.bsdroid.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class OnBootReceiver extends BroadcastReceiver {
	private static final int PERIOD = 86400000;  // 1 day
	private static final String TAG = OnBootReceiver.class.getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "HSDroid received Boot broadcast");
		AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent i = new Intent(context, OnAlarmReceiver.class);
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
		
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 15000, PERIOD, pi);
		
		Intent clearNotificationsIntent = new Intent(context, OnClearNotificationsReceiver.class);
		PendingIntent clearNotificationsPi = PendingIntent.getBroadcast(context, 1, clearNotificationsIntent, 0);
		
		mgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + 5000, PERIOD, clearNotificationsPi);
	}
}
